import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import ENV from '../../../src/environments/environment.common';
import { HttpService } from './http.service';
import { map } from 'rxjs/operators';
import { CheckBirthdayUserDto } from '../dto/check-birthday-user.dto';
import { CheckBirthdayUserResponseDto } from '../dto/check-birthday-user-response.dto';

const ENVIRONMENT = _.merge(ENV);

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private httpService: HttpService) { }

    verifyBirthdate(data: CheckBirthdayUserDto) {

        return this.httpService.postJson<CheckBirthdayUserResponseDto>(ENVIRONMENT.API_URL_SERVICE + '/user/birthdate', data)
            .pipe(map(res => {
                return res;
            }));

    }

}