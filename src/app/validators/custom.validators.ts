import {FormControl, FormGroup} from '@angular/forms';
import * as moment from 'moment';

export class CustomsValidators {
    static notBlank(control: FormControl) {


        const empty = (control.value || '').trim().length === 0;
        return !empty ? null : {
            notBlank: {
                valid: false
            }
        };
    }

    static dateDDMMYYYY(control: FormControl) {

        let val = control.value;
        if (val.day !== undefined && val.month !== undefined && val.year !== undefined) {
            val = val.day + '-' + val.month + '-' + val.year;
        }
        const date = moment(val, 'DD-MM-YYYY');
        return date.isValid() ? null : {
            dateDDMMYYYY: {
                valid: false
            }
        };
    }

    static dateMaxToday(control: FormControl) {

        let val = control.value;
        if (val.day !== undefined && val.month !== undefined && val.year !== undefined) {
            val = val.day + '-' + val.month + '-' + val.year;
        }
        const date = moment(val, 'DD-MM-YYYY');
        const now = moment();
        return !moment(date.format('DD-MM-YYYY')).isAfter(now.format('DD-MM-YYYY'), 'day') ? null : {
            dateMaxToday: {
                valid: false
            }
        };
    }
}