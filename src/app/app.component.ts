import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import { FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { CustomsValidators } from './validators/custom.validators';
import { CheckBirthdayUserDto } from './dto/check-birthday-user.dto';
import { CheckBirthdayUserResponseDto } from './dto/check-birthday-user-response.dto';
import * as moment from 'moment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    // * Form Values
    public inputForm;
    public firstname = '';
    public lastname = '';
    public birthdate = '';

    // * Status values
    public status_loading = false;
    public show_form_errors = { firstname: false, lastname: false, birthdate: false };
    public show_response = false;
    public errors = undefined;

    // * Data response
    public data: CheckBirthdayUserResponseDto;

    // * Data default
    default_congratulation = 'Today its your birthday! Congratulations!';
    now = moment();

    constructor(
        private userService: UserService,
        private formBuilder: FormBuilder,
    ) { }

    ngOnInit() {
        this.inputForm = this.formBuilder.group({
            firstname: [
                this.firstname,
                Validators.compose([
                    CustomsValidators.notBlank,
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(60)
                ])
            ],
            lastname: [
                this.lastname,
                Validators.compose([
                    CustomsValidators.notBlank,
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(60)
                ])
            ],
            birthdate: [
                this.birthdate,
                Validators.compose([
                    CustomsValidators.dateDDMMYYYY,
                    CustomsValidators.dateMaxToday
                ])
            ],
        });
    }

    /**
     * Validate form and submit to service
     */
    public async submit() {
        this.status_loading = true;
        this.show_form_errors = { firstname: true, lastname: true, birthdate: true };
        this.errors = undefined;

        if (!this.inputForm.valid) {
            this.status_loading = false;
            return;
        }
        const val = this.inputForm.get('birthdate').value;

        const formData: CheckBirthdayUserDto = {
            firstname: this.inputForm.get('firstname').value.trim(),
            lastname: this.inputForm.get('lastname').value.trim(),
            birthdate: moment(val.day + '-' + val.month + '-' + val.year, 'DD-MM-YYYY').format('DD-MM-YYYY')
        };

        await this.userService.verifyBirthdate(formData).toPromise()

            .then(res => {

                this.data = res;
                this.cleanForm();
                this.show_response = true;

            })

            .catch(e => {
                if (e === undefined) {
                    this.errors.push('The server its gone');
                    this.status_loading = false;
                    return false;
                }

                switch (e.status) {
                    case 400:
                        if (e.error !== undefined && e.error.detail !== undefined) {
                            if (Array.isArray(e.error.detail)) {
                                this.errors = e.error.detail.join('<br>');
                            } else {
                                this.errors = e.error.detail;
                            }
                        } else {
                            this.errors = 'There is a problem with your request';
                        }
                    case 500:
                        if (e.error !== undefined && e.error.detail !== undefined) {

                        } else {
                            this.errors = 'There is a problem with the server';
                        }
                        break;
                    case 404:
                        this.errors = 'Service not found';
                    default:
                        this.errors = 'This is weird... don\'t know what hapen';
                }

            });
        this.status_loading = false;
    }

    public cleanForm() {
        this.inputForm.get('firstname').setValue('');
        this.inputForm.get('lastname').setValue('');
        this.inputForm.get('birthdate').setValue('');
        this.status_loading = false;
        this.show_form_errors = { firstname: false, lastname: false, birthdate: false };
        this.show_response = false;
        this.errors = undefined;
    }

    /**
     * Check for a form error on specific field
     * @param field field to check for form errors
     * @param error error to check
     */
    public formCheck(field: string, error: string) {
        let control = this.inputForm.controls;
        switch (field) {
            case 'firstname':
                control = control.firstname;
                if (error === 'any') {
                    return (
                        control.hasError('notBlank')
                        || control.hasError('required')
                        || control.hasError('minlength')
                        || control.hasError('maxLength')
                    );
                }
                return control.hasError(error);
            case 'lastname':
                control = control.lastname;
                if (error === 'any') {
                    return (
                        control.hasError('notBlank')
                        || control.hasError('required')
                        || control.hasError('minlength')
                        || control.hasError('maxLength')
                    );
                }
                return control.hasError(error);
            case 'birthdate':
                control = control.birthdate;
                if (error === 'any') {
                    return (
                        control.hasError('dateDDMMYYYY')
                        || control.hasError('dateMaxToday')
                    );
                }
                return control.hasError(error);
        }
        return null;
    }

    /**
     * Convert a string number in a number
     * @param input number string
     */
    public toNumber(input: string) {
        if (input.trim().length === 0) {
            return NaN;
        }
        return Number(input);
    }
}
