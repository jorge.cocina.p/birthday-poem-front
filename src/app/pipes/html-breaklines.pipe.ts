import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'htmlBreaklines'})
export class HtmlBreaklines implements PipeTransform {
    transform(value: string): string {
        return value.split('\n').join('<br>');
    }
}