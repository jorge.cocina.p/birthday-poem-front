export interface CheckBirthdayUserDto {
    firstname: string;
    lastname: string;
    birthdate: string;
}