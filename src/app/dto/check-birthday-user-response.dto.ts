export interface CheckBirthdayUserResponseDto {
    firstname: string;
    lastname: string;
    birthdate: string;
    age: number;
    daysToNextBirthday?: number;
    congratulation?: string;
    poem?: Poem;
}

export interface Poem {
    title: string;
    url: string;
    content: string;
    poet: Poet;
}

export interface Poet {
    name: string;
    url: string;
    poet: Poet;
}