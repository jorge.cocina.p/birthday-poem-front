# BirthdayPoemFront

Test simple angular front.

## Requirements

- npm 6

## Install dependencies

Run `npm install` to donwload required dependencies.

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to [http://localhost:4200](http://localhost:4200/)

## Build (Not required)

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

